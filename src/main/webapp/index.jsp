<%-- 
    Document   : index
    Created on : 03-jul-2021, 19:52:31
    Author     : Nayel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Solemne 3</title>
        <style type="text/css">
            body{
                font-family: verdana;
                font-size: 11px;
                color:#505050;
                background: rgb(135,238,244);
                background: linear-gradient(90deg, rgba(135,238,244,0.9178046218487395) 0%, rgba(172,214,245,1) 35%, rgba(255,255,255,1) 100%);
            }
        </style>
    </head>
    <body>
        <table width="500" cellpadding="2" cellspacing="1" align="center">
            <thead>
            <th align="center">
                <h1>Solemne 3 nayeli loncomilla</h1>
            </th>
        </thead>
        <tr>
            <td>
                <b>Listar:</b> https://solemne77.herokuapp.com/api/autos
            </td>
        </tr>
        <tr>
            <td>
                <b>Crear:</b> https://solemne77.herokuapp.com/api/autos
            </td>
        </tr>
        <tr>
            <td>
                <b>Editar:</b> https://solemne77.herokuapp.com/api/autos
            </td>
        </tr>
        <tr>
            <td>
                <b>Buscar:</b> https://solemne77.herokuapp.com/api/autos/52522
            </td>
        </tr>
        <tr>
            <td>
               <b>Eliminar:</b> https://solemne77.herokuapp.com/api/autos/52522
            </td>
        </tr>
    </table>
</body>
</html>
