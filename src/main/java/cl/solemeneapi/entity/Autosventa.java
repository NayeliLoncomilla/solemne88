/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.solemeneapi.entity;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author GaCTuS
 */
@Entity
@Table(name = "autosventa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Autosventa.findAll", query = "SELECT a FROM Autosventa a"),
    @NamedQuery(name = "Autosventa.findByPatente", query = "SELECT a FROM Autosventa a WHERE a.patente = :patente"),
    @NamedQuery(name = "Autosventa.findByMarca", query = "SELECT a FROM Autosventa a WHERE a.marca = :marca"),
    @NamedQuery(name = "Autosventa.findByModelo", query = "SELECT a FROM Autosventa a WHERE a.modelo = :modelo"),
    @NamedQuery(name = "Autosventa.findByAno", query = "SELECT a FROM Autosventa a WHERE a.ano = :ano"),
    @NamedQuery(name = "Autosventa.findByValor", query = "SELECT a FROM Autosventa a WHERE a.valor = :valor")})
public class Autosventa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "patente")
    private Integer patente;
    @Size(max = 2147483647)
    @Column(name = "marca")
    private String marca;
    @Size(max = 2147483647)
    @Column(name = "modelo")
    private String modelo;
    @Size(max = 2147483647)
    @Column(name = "ano")
    private String ano;
    @Column(name = "valor")
    private BigInteger valor;

    public Autosventa() {
    }

    public Autosventa(Integer patente) {
        this.patente = patente;
    }

    public Integer getPatente() {
        return patente;
    }

    public void setPatente(Integer patente) {
        this.patente = patente;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public BigInteger getValor() {
        return valor;
    }

    public void setValor(BigInteger valor) {
        this.valor = valor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (patente != null ? patente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Autosventa)) {
            return false;
        }
        Autosventa other = (Autosventa) object;
        if ((this.patente == null && other.patente != null) || (this.patente != null && !this.patente.equals(other.patente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.solemeneapi.entity.Autosventa[ patente=" + patente + " ]";
    }
    
}
